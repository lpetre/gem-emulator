#!/bin/bash

# Configuration variables
image_repo="gitlab-registry.cern.ch/lpetre/gem-emulator"
cni_config_dir="/etc/cni/gem-emulator.d"
podman="podman --cni-config-dir ${cni_config_dir}"
piddir="/run/gem-emulator"

# Usage
if [[ $# != 2 ]]; then
    echo "Incorrect usage: ${0} <command> <instance name>"
    exit 1
fi

# Parse instance name
if [[ ${2} =~ ^([[:digit:]]+)-([[:digit:]]+):([^:]+):([^:]+)$ ]]; then
    req_instance=${2}
    req_shelf=${BASH_REMATCH[1]}
    req_slot=${BASH_REMATCH[2]}
    req_image=${BASH_REMATCH[3]}
    req_release=${BASH_REMATCH[4]}
else
    echo "Error: Instance name does not follow the shelf-slot:image:release format."
    exit 1
fi

# Build container parameters
container_pidfile="${piddir}/${req_instance}.pid"
container_image="${image_repo}/${req_image}:${req_release}"
container_name="${req_image}-shelf$(printf %02d ${req_shelf})-slot$(printf %02d ${req_slot})"
container_ip="192.168.${req_shelf}.$((${req_slot}+40))"
container_ipbus_connection="chtcp-2.0://gem-shelf$(printf %02d ${req_shelf})-controlhub:10203?target=192.168.80.${req_slot}:50001"

# Start the container
if [[ "${1}" = "start" ]]; then

    echo "Starting container on shelf ${req_shelf}, slot ${req_slot} (${container_ip})"

    # Check that the IP address is not in use on the local machine
    _container_file_ip="/var/lib/cni/networks/gem-emulator/${container_ip}"
    if [[ -e ${_container_file_ip} ]]; then
        # First line contains the container ID
        # Note that EOL are CRLF
        IFS=$'\r\n' read -r _container_using_ip < ${_container_file_ip}
        _running_containers=$(podman container ls -q --no-trunc)

        if [[ ! "${_running_containers}" =~ (^|[[:space:]])"${_container_using_ip}"($|[[:space:]]) ]]; then
            echo "IP not in use by any running container, removing file."
            rm "${_container_file_ip}"
        else
            echo "Container using the same IP address already exists on the machine."
            exit 1
        fi
    fi

    # Nor on the network
    # Get the interface for the ARP ping
    _route=$(ip route get ${container_ip})
    if [[ ! "${_route}" =~ ^.*[[:space:]]dev[[:space:]]([^[:space:]]*).*$ ]]; then
        echo "Error: cannot parse route: ${_route}."
        exit 1
    fi
    _interface=${BASH_REMATCH[1]}

    if ! arping -q -D -I ${_interface} -c1 ${container_ip}; then
        echo "IP already used and not managed by the local GEM emulator."
        exit 1
    fi

    # Create PID file directory
    mkdir -p ${piddir}

    # Remove any dead container
    eval ${podman} container rm ${container_name} 2>/dev/null || true

    # And launch the container!
    eval ${podman} container run \
                   --rm -d \
                   --conmon-pidfile ${container_pidfile} \
                   --name ${container_name} \
                   --hostname ${container_name} \
                   --ip ${container_ip} \
                   --cap-add audit_control \
                   -v ${container_name}:/mnt/persistent \
                   -v /opt:/opt:ro \
                   --env GEM_IPBUS_CONNECTION=${container_ipbus_connection} \
                   ${container_image}

# Stop the container
elif [[ "${1}" = "stop" ]]; then

    eval ${podman} container stop ${container_name}

    # Wait for container to be stopped
    # Prevents systemd restart errors
    eval ${podman} container wait ${container_name} 2>/dev/null || true

    # Delete PID file directory if empty
    rmdir --ignore-fail-on-non-empty ${piddir}

else
    echo "Error: Unknown command (${1})."
    exit 1
fi
