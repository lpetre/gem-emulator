# Software emulator for the GEM backends

This repository hosts the software required for the GEM backend emulators.
It consists of two parts:

1. A `systemd` unit packaged as a RPM.
   It is used for spawning emulators with sensible default parameters.
2. A set of container images providing the tools emulating the µTCA card software.
   The images are downloaded from the CERN GitLab registry as needed.

## Installation

The RPM can be installed from the official Yum repository.
A typical `.repo` file is found under the `doc` directory.

Once the new repository is configured, the RPM packages are available through the Yum package manager:

``` bash
# yum install gem-emulator
```

If `selinux` is enabled on the system (default on CentOS), the related `selinux` policy should also be installed:

``` bash
# yum install gem-emulator-selinux
```

### Network configuration

The container network is based on `macvlan` interfaces which allow for a flat network topology with the private network.

Before starting a container, the network interface name must be configured in `/etc/cni/gem-emulator.d/00-gem-emulator.conflist`.
The `master` field must be set to the **physical interface** name connected to the private network.

Note that the container host cannot communicate with the containers without a special setup.
This is due to the Linux kernel preventing communication between the default namespace and other namespaces.
On CentOS the recommended method to setup communication between the host and the containers is to setup a host `macvlan` with NetworkManager.

### IPBus ControlHub

Currently all containers use IPBus to connect to the hardware.
An IPBus ControlHub must therefore be available on the network and reachable under the `gem-shelfXX-controlhub` name.

For performance reasons, it is recommended for the control hub to be started on the same machine as the machine which hosts the containers. 

## Usage

### Starting the containers

Containers are started with `systemctl`.
For example the following command starts a GLIB-based emulator, version 0, on shelf 1 and slot 9:

``` bash
# systemctl start gem-emulator@1-9:glib:0
```

The `systemd` unit instances follow the `shelf-slot:image:release` format.

If missing, the container image is downloaded on-the-fly.
Therefore the first container instantiation can take more time than usual.

### Connecting to the containers

Once started, the container can be accessed through SSH via the usual geographical IP address.
A `gemuser` account is already setup without password.

``` bash
$ ssh gemuser@gem-shelfXX-slotYY
```
