%global selinuxtype targeted

Name:          gem-emulator
Version:       0.2.1
Release:       1%{?dist}
BuildArch:     noarch
Summary:       Provides a systemd unit managing a configurable GEM emulator

Group:         Unspecified
License:       MIT
URL:           https://gitlab.cern.ch/lpetre/gem-emulator
Source0:       %{name}-%{version}.tar.gz

Requires:      podman
Requires:      iputils

%if 0%{?rhel}
BuildRequires: systemd
%else
BuildRequires: systemd-rpm-macros
%endif

%{?systemd_requires}

%description
To be written...

# SELinux policy seems broken on CentOS 7
# Let's add the required transitions ourself
# Inspired by:
# * https://fedoraproject.org/wiki/SELinux_Policy_Modules_Packaging_Draft
# * https://lukas-vrabec.com/index.php/2017/03/18/using-rpm-macros-in-product-selinux-subpackages/
%package selinux
Summary:       SELinux policy for the GEM emulator (required on CentOS 7)

BuildRequires: selinux-policy-devel
%if "%{_selinux_policy_version}" != ""
Requires:      selinux-policy >= %{_selinux_policy_version}
%endif
Requires(post):   /usr/sbin/semodule
Requires(postun): /usr/sbin/semodule

Requires:      %{name} = %{version}-%{release}

%description selinux
To be written...

%prep
%setup -q

%build
cd package/selinux
make NAME=${selinuxtype} -f /usr/share/selinux/devel/Makefile

%install
install -m 644 -D systemd/00-gem-emulator.conflist %{buildroot}/etc/cni/gem-emulator.d/00-gem-emulator.conflist
install -m 755 -D systemd/gem-emulator.sh %{buildroot}/usr/libexec/gem-emulator.sh
install -m 644 -D systemd/gem-emulator@.service %{buildroot}%{_unitdir}/gem-emulator@.service

install -d %{buildroot}%{_datadir}/selinux/packages
install -m 644 package/selinux/%{name}.pp %{buildroot}%{_datadir}/selinux/packages

%files
%dir /etc/cni/gem-emulator.d
%config(noreplace) /etc/cni/gem-emulator.d/00-gem-emulator.conflist
/usr/libexec/gem-emulator.sh
%{_unitdir}/gem-emulator@.service

%post
%systemd_post

%preun
%systemd_preun

%postun
%systemd_postun

%files selinux
%{_datadir}/selinux/packages/%{name}.pp

%post selinux
%selinux_modules_install -s %{selinuxtype} %{_datadir}/selinux/packages/%{name}.pp

%postun selinux
%selinux_modules_uninstall -s %{selinuxtype} %{name}

%changelog
* Fri Jan 17 2020 Laurent Pétré <lpetre@ulb.ac.be> 0.2.1-1
- Add missing ports in GEM_IPBUS_CONNECTION (lpetre@ulb.ac.be)

* Sun Dec 15 2019 Laurent Pétré <lpetre@ulb.ac.be> 0.2.0-1
- Improve systemd instance name parsing (lpetre@ulb.ac.be)
- Use host name for the IPBus ControlHub (lpetre@ulb.ac.be)
- Remove container image release from container name (lpetre@ulb.ac.be)
- Lower podman log level in systemd unit (lpetre@ulb.ac.be)

* Sat Dec 14 2019 Laurent Pétré <lpetre@ulb.ac.be> 0.1.0-2
- Split the SELinux package (lpetre@ulb.ac.be)
- Add a SELinux policy module (lpetre@ulb.ac.be)

* Fri Dec 13 2019 Laurent Pétré <lpetre@ulb.ac.be> 0.1.0-1
- First release

