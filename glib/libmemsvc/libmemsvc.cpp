#include "libmemsvc.h"

#include "uhal/ClientFactory.hpp"

#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef __cplusplus
    #define DLLEXPORT extern "C"
#endif

struct memsvc_handle {
    char last_error[512];
    boost::shared_ptr<uhal::ClientInterface> uhalClient;
};

static inline void set_error(memsvc_handle_t svc, const char *user, int err)
{
    memset(svc->last_error, 0, 512);
    if (user)
        strncpy(svc->last_error, user, 512);
    if (err) {
        char errbuf[512];
        errno = 0;
        strerror_r(err, errbuf, 512);
        errbuf[511] = '\0';

        strncpy(svc->last_error+strlen(svc->last_error), errbuf, 512-strlen(svc->last_error));
        svc->last_error[511] = '\0';
    }
}

DLLEXPORT const char *memsvc_get_last_error(memsvc_handle_t svc)
{
    return static_cast<const char *>(svc->last_error);
}

DLLEXPORT int memsvc_open(memsvc_handle_t *handle)
{
    struct memsvc_handle *svc = new struct memsvc_handle;
    *handle = svc;

    // Any exception is a failure
    try {
        // Load address from environment variable
        if (const char* URI = std::getenv("GEM_IPBUS_CONNECTION")) {
            svc->uhalClient = uhal::ClientFactory::getInstance().getClient("GLIB", URI);
        } else {
            set_error(svc, "Error: GEM_IPBUS_CONNECTION environment variable not defined", 0);
            return -1;
        }
    }
    catch (const std::exception &e) {
        set_error(svc, e.what(), 0);
        return -1;
    }

    return 0;
}

DLLEXPORT int memsvc_close(memsvc_handle_t *svc)
{
    delete *svc;
    *svc = NULL;

    return 0;
}

DLLEXPORT int memsvc_read(memsvc_handle_t svc, uint32_t addr, uint32_t words, uint32_t *data)
{
    // Convert the address from libmemsvc address space to IpBus address space
    addr = ((addr - 0x64000000) >> 2) + 0x40000000;

    // Any exception is a failure
    try {
        auto _data = svc->uhalClient->readBlock(addr, words);
        svc->uhalClient->dispatch();
        std::copy(_data.begin(), _data.end(), data);
    }
    catch (const std::exception &e) {
        set_error(svc, e.what(), 0);
        return -1;
    }

    return 0;
}

DLLEXPORT int memsvc_write(memsvc_handle_t svc, uint32_t addr, uint32_t words, const uint32_t *data)
{
    // Convert the address from libmemsvc address space to IpBus address space
    addr = ((addr - 0x64000000) >> 2) + 0x40000000;

    // Any exception is a failure
    try {
        std::vector<uint32_t> _data(data, data + words);

        svc->uhalClient->writeBlock(addr, _data);
        svc->uhalClient->dispatch();
    }
    catch (const std::exception &e) {
        set_error(svc, e.what(), 0);
        return -1;
    }

    return 0;
}

