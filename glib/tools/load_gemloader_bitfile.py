#!/bin/env python

import uhal

import os
import sys
import struct

glibAddr = os.environ["GEM_IPBUS_CONNECTION"]
glib = uhal.buildClient("GLIB", glibAddr)

# Read the file
with open(sys.argv[1], "rb") as f:
    bitstream = []

    while True:
        value = f.read(4)
        if len(value) != 4:
            break

        value = struct.unpack('>I', value)[0]
        bitstream.append(value)

print("Bitstream size : ", len(bitstream)*4)

# Take control over the SRAM
glib.write(0x8, 0)
glib.dispatch()

# Write the bistream
glib.writeBlock(0x02000000, bitstream)
glib.dispatch()

# Check the write
readback = glib.readBlock(0x02000000, len(bitstream))
glib.dispatch()

if list(bitstream) == list(readback):
    print "File correctly written."
else:
    print "Error during readback."

# Release control of the SRAM
glib.write(0x8, 1)
glib.dispatch()

