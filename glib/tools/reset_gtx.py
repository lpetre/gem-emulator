#!/bin/env python

import uhal

import os

glibAddr = os.environ["GEM_IPBUS_CONNECTION"]
glib = uhal.buildClient("GLIB", glibAddr)

#### Configure the GTX's
## GBT: invert TX polarity
for i in range(4):
    glib.write(0x50000000+2*i, 0x00040088)

## Trigger: only reset RX
for i in range(4):
    glib.write(0x50000008+2*i, 0x00040000)

glib.dispatch()

## Release resets
for i in range(4):
    glib.write(0x50000000+2*i, 0x00000080)

for i in range(4):
    glib.write(0x50000008+2*i, 0x00000000)

glib.dispatch()
