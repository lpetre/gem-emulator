#!/bin/sh

for algo in rsa ecdsa ed25519
do
  if [ ! -e /mnt/persistent/config/ssh_host_${algo}_key ]
  then
    ssh-keygen -N "" -C "" -t ${algo} -f /mnt/persistent/config/ssh_host_${algo}_key
  fi
done

env | grep ^GEM_ >> /etc/environment

exec /usr/sbin/sshd -D -e
